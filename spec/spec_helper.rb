# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'
require 'simplecov'
SimpleCov.start 'rails'
require 'database_cleaner'
require_relative '../dependencies'
require 'rspec'
require 'rack/test'
require 'shoulda/matchers'
Dir[File.dirname(__FILE__) + '/support/**/*.rb'].sort.each { |f| require f }

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :active_record
    with.library :active_model
  end
end

RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.include RSpec::Benchmark::Matchers
  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end
  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
  FactoryBot.find_definitions

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
end
