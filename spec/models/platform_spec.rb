# frozen_string_literal: true

# == Schema Information
#
# Table name: platforms
#
#  id  :bigint           not null, primary key
#  uid :string
#

describe Platform, type: :model do
  it { is_expected.to validate_presence_of(:uid) }
  it { is_expected.to validate_uniqueness_of(:uid) }
  it { is_expected.to have_many(:players) }
  it { is_expected.to have_many(:bets).through(:players) }
  it { is_expected.to have_many(:tokens).through(:players) }
end
