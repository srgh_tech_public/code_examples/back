# frozen_string_literal: true

# == Schema Information
#
# Table name: reels
#
#  id          :bigint           not null, primary key
#  game_rtp_id :bigint
#  slot_ids    :bigint           default([]), not null, is an Array
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

describe Reel, type: :model do
  it { is_expected.to belong_to(:game_rtp).required(true) }
  it { is_expected.to have_many(:reel_slots) }
  it { is_expected.to have_many(:positioned_slots).through(:reel_slots) }
  it { is_expected.to have_one(:game).through(:game_rtp) }
  describe 'validate slot_should_belong_to_game' do
    let(:second_game) { create(:game) }
    let!(:bad_slot) { create(:slot, game: second_game) }
    let(:game_rtp) { create(:game_rtp) }
    let(:valid_slot) { create(:slot, game: game_rtp.game) }
    let!(:reel) { game_rtp.reels.first }

    it { expect(reel).to allow_value([valid_slot.id]).for(:slot_ids) }
    it { expect(reel).not_to allow_value([bad_slot.id]).for(:slot_ids) }
    it {
      expect(reel).not_to allow_value([bad_slot.id, valid_slot.id])
        .for(:slot_ids)
    }
  end

  describe 'validate slot_allowed_to_be_on_reel' do
    let(:game_rtp) { create(:game_rtp) }
    let(:game) { game_rtp.game }
    let(:valid_slot) { create(:slot, game: game) }
    let!(:reel) { game_rtp.reels.first }
    let!(:bad_slot) { create :slot, denied_reel_positions: [1], game: game }
    it { expect(reel).to allow_value([valid_slot.id]).for(:slot_ids) }
    it { expect(reel).not_to allow_value([bad_slot.id]).for(:slot_ids) }
    it {
      expect(reel).not_to allow_value([bad_slot.id, valid_slot.id])
        .for(:slot_ids)
    }
  end
end
