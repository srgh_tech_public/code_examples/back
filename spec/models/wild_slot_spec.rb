# frozen_string_literal: true

# == Schema Information
#
# Table name: wild_slots
#
#  wild_id        :bigint
#  simple_slot_id :integer
#

describe WildSlot, type: :model do
  it {
    is_expected.to belong_to(:simple_slot)
      .class_name('Slot')
  }
  it { is_expected.to belong_to(:wild).class_name('Slot') }
end
