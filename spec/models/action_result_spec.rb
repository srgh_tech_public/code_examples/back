# frozen_string_literal: true

# == Schema Information
#
# Table name: action_results
#
#  id         :bigint           not null, primary key
#  action_id  :bigint
#  data       :json
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

describe ActionResult, type: :model do
  it { is_expected.to belong_to(:action) }
end
