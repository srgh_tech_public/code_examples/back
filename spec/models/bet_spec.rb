# frozen_string_literal: true

# == Schema Information
#
# Table name: bets
#
#  id         :bigint           not null, primary key
#  game_id    :bigint
#  player_id  :bigint
#  uid        :string
#  state      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

describe Bet, type: :model do
  it { is_expected.to belong_to(:player).required(true) }
end
