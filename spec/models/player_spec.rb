# frozen_string_literal: true

# == Schema Information
#
# Table name: players
#
#  id          :bigint           not null, primary key
#  username    :string
#  platform_id :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

describe Player, type: :model do
  it { is_expected.to validate_presence_of(:username) }
  it {
    is_expected.to validate_uniqueness_of(:username)
      .scoped_to(:platform_id)
  }
  it { is_expected.to belong_to(:platform).required(true) }
  it { is_expected.to have_many(:tokens).dependent(:destroy) }
  it { is_expected.to have_many(:bets) }
  it { is_expected.to have_many(:actions).through(:bets) }
  it { is_expected.to have_many(:action_results).through(:actions) }
end
