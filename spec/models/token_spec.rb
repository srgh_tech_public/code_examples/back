# frozen_string_literal: true

# == Schema Information
#
# Table name: tokens
#
#  id          :bigint           not null, primary key
#  player_id   :bigint
#  platform_id :bigint
#  value       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

describe Token, type: :model do
  it {
    is_expected.to validate_uniqueness_of(:value)
  }
  it { is_expected.to belong_to(:player).required(true) }
  it { is_expected.to belong_to(:platform).required(true) }
  it { is_expected.to have_many(:actions).dependent(:destroy) }
end
