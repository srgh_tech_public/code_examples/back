# frozen_string_literal: true

# == Schema Information
#
# Table name: reel_slots
#
#  id                 :bigint
#  reel_id            :bigint
#  positioned_slot_id :bigint
#

describe ReelSlot, type: :model do
  it { is_expected.to belong_to(:reel) }
  it { is_expected.to belong_to(:positioned_slot) }
end
