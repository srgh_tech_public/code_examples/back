# frozen_string_literal: true

# == Schema Information
#
# Table name: actions
#
#  id         :bigint           not null, primary key
#  bet_id     :bigint
#  token_id   :bigint
#  data       :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

describe Action, type: :model do
  it { is_expected.to belong_to(:bet).required(true) }
  it { is_expected.to belong_to(:token).required(true) }
  it { is_expected.to have_one(:action_result) }
end
