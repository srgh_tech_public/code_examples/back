# frozen_string_literal: true

# == Schema Information
#
# Table name: slots
#
#  id                    :bigint           not null, primary key
#  name                  :string
#  uid                   :string
#  game_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  slot_ids              :integer          default([]), not null, is an Array
#  line_multipliers      :integer          default([]), not null, is an Array
#  scatter_multipliers   :integer          default([]), not null, is an Array
#  denied_reel_positions :integer          default([]), not null, is an Array
#

describe Slot, type: :model do
  let(:game) { create(:game) }
  let(:slot) { create :slot, game: game }
  let(:other_slot) { create :slot, game: game }
  let(:wild) { create(:slot, game: game, slot_ids: [other_slot.id]) }

  it { is_expected.to belong_to(:game).required }
  it { is_expected.to have_many(:reel_slots) }
  it { is_expected.to have_many(:reels).through(:reel_slots) }
  it { is_expected.to have_many(:wild_slots).class_name('WildSlot') }
  it { is_expected.to have_many(:w_slots).class_name('WildSlot') }

  it {
    is_expected.to have_many(:wilds).through(:w_slots).class_name('Slot')
  }
  it {
    is_expected.to have_many(:simple_slots).through(:wild_slots)
                                           .class_name('Slot')
  }
  describe 'scopes' do
    let!(:scatter) { create :slot, game: game, scatter_multipliers: [nil, 1] }
    let!(:wild) { create(:slot, game: game, slot_ids: [other_slot.id]) }
    it 'expect scatters scope to include only scatter' do
      expect(described_class.scatters).to match_array([scatter])
    end
    it 'expect wilds scope to include only scatter' do
      expect(described_class.wilds).to match_array([wild])
    end
  end
  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_uniqueness_of(:name).scoped_to(:game_id) }
    it { is_expected.to validate_presence_of(:uid) }
    it { is_expected.to validate_uniqueness_of(:uid).scoped_to(:game_id) }

    describe '.substitutes?(slot)' do
      it { expect(wild.substitutes?(other_slot)).to be_truthy }
      it { expect(wild.substitutes?(slot)).to be_falsy }
      it { expect(slot.substitutes?(wild)).to be_falsy }
      it { expect(slot.substitutes?(other_slot)).to be_falsy }
      it { expect(other_slot.substitutes?(wild)).to be_falsy }
      it { expect(other_slot.substitutes?(slot)).to be_falsy }
    end

    describe '.max_scatter_multipliers' do
      it {
        expect(slot).to allow_value([])
          .for(:scatter_multipliers)
      }
      it {
        expect(slot).to allow_value([nil, nil, nil, nil, 17])
          .for(:scatter_multipliers)
      }
      it {
        expect(slot).to allow_value([1, 5, 2, 10, 17])
          .for(:scatter_multipliers)
      }
      it {
        expect(slot).to allow_value([1, 5, 2, nil, nil])
          .for(:scatter_multipliers)
      }
      it {
        expect(slot).not_to allow_value([nil, nil, nil, nil, nil, nil])
          .for(:scatter_multipliers)
      }
    end

    describe '.max_line_multipliers' do
      it {
        expect(slot).to allow_value([])
          .for(:line_multipliers)
      }
      it {
        expect(slot).to allow_value([nil, nil, nil, nil, 17])
          .for(:line_multipliers)
      }
      it {
        expect(slot).to allow_value([1, 5, 2, 10, nil])
          .for(:line_multipliers)
      }
      it {
        expect(slot).to allow_value([1, 5, 2, nil, nil])
          .for(:line_multipliers)
      }
      it {
        expect(slot).not_to allow_value([nil, nil, nil, nil, nil, nil])
          .for(:line_multipliers)
      }
      it {
        expect(slot).not_to allow_value([20, 20, 20, 20, 20, 20])
          .for(:line_multipliers)
      }
    end
  end
end
