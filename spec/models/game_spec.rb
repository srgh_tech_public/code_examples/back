# frozen_string_literal: true

# == Schema Information
#
# Table name: games
#
#  id         :bigint           not null, primary key
#  name       :string
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

describe Game, type: :model do
  let(:game) { create :game }
  it { expect(game).to validate_presence_of(:name) }
  it { expect(game).to validate_uniqueness_of(:name) }
  it { expect(game).to validate_presence_of(:uid) }
  it { expect(game).to validate_uniqueness_of(:uid) }
  it { expect(game).to have_many(:slots) }
  it { expect(game).to have_many(:wilds) }
  it { expect(game).to have_many(:scatters) }
  it { expect(game).to have_many(:game_rtps) }
  it { expect(game).to have_many(:reels).through(:game_rtps) }
  it { expect(game.rows_qty).to eq 3 }
  it { expect(game.reels_qty).to eq 5 }
end
