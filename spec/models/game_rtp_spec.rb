# frozen_string_literal: true

# == Schema Information
#
# Table name: game_rtps
#
#  id         :bigint           not null, primary key
#  game_id    :integer
#  active     :boolean          default(FALSE)
#  rtp        :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  details    :jsonb
#

describe GameRtp, type: :model do
  it { is_expected.to validate_presence_of(:rtp) }
  it {
    is_expected.to validate_numericality_of(:rtp)
      .is_greater_than(50)
      .is_less_than(110)
  }
  it { is_expected.to belong_to(:game).required(true) }
  it { is_expected.to have_many(:reels) }
  it { is_expected.to have_db_column(:details).of_type(:jsonb) }
  describe 'instance methods' do
    describe '.game_slots' do
      let!(:game) { create(:game) }
      it {}
    end
  end
end
