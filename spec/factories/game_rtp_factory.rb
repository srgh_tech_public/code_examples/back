# frozen_string_literal: true

# == Schema Information
#
# Table name: game_rtps
#
#  id         :bigint           not null, primary key
#  game_id    :integer
#  active     :boolean          default(FALSE)
#  rtp        :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  details    :jsonb
#

FactoryBot.define do
  factory :game_rtp do
    rtp { rand(60...105) }
    association :game
  end
end
