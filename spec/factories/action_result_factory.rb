# frozen_string_literal: true

# == Schema Information
#
# Table name: action_results
#
#  id         :bigint           not null, primary key
#  action_id  :bigint
#  data       :json
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :action_result do
    action
    data {}
    status { 0 }
  end
end
