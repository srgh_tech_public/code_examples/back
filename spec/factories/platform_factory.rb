# frozen_string_literal: true

# == Schema Information
#
# Table name: platforms
#
#  id  :bigint           not null, primary key
#  uid :string
#

FactoryBot.define do
  factory :platform do
    uid { SecureRandom.hex }
  end
end
