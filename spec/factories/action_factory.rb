# frozen_string_literal: true

# == Schema Information
#
# Table name: actions
#
#  id         :bigint           not null, primary key
#  bet_id     :bigint
#  token_id   :bigint
#  data       :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :action do
    bet
    data {}
  end
end
