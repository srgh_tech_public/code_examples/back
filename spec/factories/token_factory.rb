# frozen_string_literal: true

# == Schema Information
#
# Table name: tokens
#
#  id          :bigint           not null, primary key
#  player_id   :bigint
#  platform_id :bigint
#  value       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :token do
    player
    value { SecureRandom.hex }
  end
end
