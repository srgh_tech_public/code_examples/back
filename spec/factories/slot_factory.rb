# frozen_string_literal: true

# == Schema Information
#
# Table name: slots
#
#  id                    :bigint           not null, primary key
#  name                  :string
#  uid                   :string
#  game_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  slot_ids              :integer          default([]), not null, is an Array
#  line_multipliers      :integer          default([]), not null, is an Array
#  scatter_multipliers   :integer          default([]), not null, is an Array
#  denied_reel_positions :integer          default([]), not null, is an Array
#

FactoryBot.define do
  factory :slot do
    name { Faker::Name.name_with_middle }
    uid { SecureRandom.hex }
    association :game
  end
end
