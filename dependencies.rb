# frozen_string_literal: true

require 'bundler'

RACK_ENV ||= (ENV['RACK_ENV'] || :development).to_s
Bundler.require(:default, RACK_ENV)

require 'zeitwerk'

APP_LOADER = Zeitwerk::Loader.new
%w[
  lib
  app
  app/models
  app/services
].each(&APP_LOADER.method(:push_dir))

APP_LOADER.setup
APP_LOADER.eager_load
if RACK_ENV != 'production'
  ActiveRecord::Base.logger = Logger.new("./log/ar_#{RACK_ENV}.log")
end
