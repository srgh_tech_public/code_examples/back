# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.6'

gem 'activerecord'
gem 'pg'
gem 'puma'
gem 'rack'
gem 'rack-contrib'
gem 'rake'
gem 'redis'
gem 'sidekiq', '~> 5'
gem 'sidekiq-scheduler'
gem 'sinatra'
gem 'sinatra-activerecord'
gem 'sinatra-contrib'
gem 'websocket-driver'
gem 'zeitwerk'

group :test do
  gem 'database_cleaner'
  gem 'factory_bot'
  gem 'faker'
  gem 'guard'
  gem 'guard-annotate', require: false
  gem 'guard-rspec', require: false
  gem 'guard-rubocop', require: false
  gem 'rack-test'
  gem 'rspec'
  gem 'rspec-benchmark'
  gem 'rspec-nc', require: false
  gem 'shoulda-matchers'
  gem 'simplecov'
end

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'annotate', git: 'https://github.com/ctran/annotate_models.git'
  gem 'rubocop', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
end
