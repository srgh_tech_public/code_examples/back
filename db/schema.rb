# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_04_182109) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_results", force: :cascade do |t|
    t.bigint "action_id"
    t.json "data"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "actions", force: :cascade do |t|
    t.bigint "bet_id"
    t.bigint "token_id"
    t.json "data"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "bets", force: :cascade do |t|
    t.bigint "game_id"
    t.bigint "player_id"
    t.string "uid"
    t.string "state"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "game_rtps", force: :cascade do |t|
    t.integer "game_id"
    t.boolean "active", default: false
    t.decimal "rtp"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "details", default: {}
    t.index ["game_id"], name: "index_game_rtps_on_game_id"
  end

  create_table "games", force: :cascade do |t|
    t.string "name"
    t.string "uid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_games_on_name"
    t.index ["uid"], name: "index_games_on_uid"
  end

  create_table "platforms", force: :cascade do |t|
    t.string "uid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "players", force: :cascade do |t|
    t.string "username"
    t.bigint "platform_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reels", force: :cascade do |t|
    t.bigint "game_rtp_id"
    t.bigint "slot_ids", default: [], null: false, array: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "slots", force: :cascade do |t|
    t.string "name"
    t.string "uid"
    t.integer "game_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "slot_ids", default: [], null: false, array: true
    t.integer "line_multipliers", default: [], null: false, array: true
    t.integer "scatter_multipliers", default: [], null: false, array: true
    t.integer "denied_reel_positions", default: [], null: false, array: true
    t.index ["name"], name: "index_slots_on_name"
    t.index ["uid"], name: "index_slots_on_uid"
  end

  create_table "tokens", force: :cascade do |t|
    t.bigint "player_id"
    t.bigint "platform_id"
    t.string "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
