# frozen_string_literal: true

class CreateTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :tokens do |t|
      t.bigint :player_id
      t.bigint :platform_id
      t.string :value
      t.timestamps
    end
  end
end
