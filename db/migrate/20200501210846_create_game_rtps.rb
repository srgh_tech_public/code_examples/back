# frozen_string_literal: true

class CreateGameRtps < ActiveRecord::Migration[6.0]
  def change
    create_table :game_rtps do |t|
      t.integer :game_id, index: true
      t.boolean :active, default: false
      t.decimal :rtp
      t.timestamps
    end
  end
end
