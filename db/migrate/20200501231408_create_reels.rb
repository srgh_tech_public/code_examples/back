# frozen_string_literal: true

class CreateReels < ActiveRecord::Migration[6.0]
  def change
    create_table :reels do |t|
      t.bigint :game_rtp_id
      t.bigint :slot_ids, array: true, default: [], null: false
      t.timestamps
    end
  end
end
