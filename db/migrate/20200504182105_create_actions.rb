# frozen_string_literal: true

class CreateActions < ActiveRecord::Migration[6.0]
  def change
    create_table :actions do |t|
      t.bigint :bet_id
      t.bigint :token_id
      t.json :data
      t.timestamps
    end
  end
end
