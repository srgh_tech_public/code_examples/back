# frozen_string_literal: true

class AddSubstituteIdsToSlot < ActiveRecord::Migration[6.0]
  def up
    add_column :slots, :slot_ids, :integer,
               array: true, default: [], null: false
    connection.execute %(
      CREATE OR REPLACE VIEW wild_slots
      AS
        SELECT  slots.id as wild_id, UNNEST(slots.slot_ids) as simple_slot_id
        FROM slots
    )
  end

  def down
    connection.execute %(
      DROP VIEW IF EXISTS wild_slots
    )
    remove_column :slots, :slot_ids
  end
end
