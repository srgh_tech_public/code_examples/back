# frozen_string_literal: true

class CreateActionResults < ActiveRecord::Migration[6.0]
  def change
    create_table :action_results do |t|
      t.bigint :action_id
      t.json :data
      t.integer :status
      t.timestamps
    end
  end
end
