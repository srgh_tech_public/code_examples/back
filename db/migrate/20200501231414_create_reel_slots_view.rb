# frozen_string_literal: true

class CreateReelSlotsView < ActiveRecord::Migration[6.0]
  def up
    connection.execute %(
      CREATE OR REPLACE VIEW reel_slots
      AS
        SELECT  ordinality as id, reels.id as reel_id, positioned_slot_id
        FROM reels, UNNEST(reels.slot_ids) with ordinality positioned_slot_id
      ORDER by reel_id, id)
  end

  def down
    connection.execute %(
      DROP VIEW reel_slots
    )
  end
end
