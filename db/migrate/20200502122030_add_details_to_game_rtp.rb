# frozen_string_literal: true

class AddDetailsToGameRtp < ActiveRecord::Migration[6.0]
  def change
    add_column :game_rtps, :details, :jsonb, default: {}
  end
end
