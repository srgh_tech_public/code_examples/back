# frozen_string_literal: true

class AddLineMultipliersToSlots < ActiveRecord::Migration[6.0]
  def change
    add_column :slots, :line_multipliers, :integer,
               array: true, default: [], null: false
    add_column :slots, :scatter_multipliers, :integer,
               array: true, default: [], null: false
    add_column :slots, :denied_reel_positions, :integer,
               array: true, default: [], null: false
  end
end
