# frozen_string_literal: true

class CreatePlayersActionsTokens < ActiveRecord::Migration[6.0]
  def change
    create_table :platforms do |t|
      t.string :uid
      t.timestamps
    end

    create_table :players do |t|
      t.string :username
      t.bigint :platform_id
      t.timestamps
    end
  end
end
