# frozen_string_literal: true

class CreateBets < ActiveRecord::Migration[6.0]
  def change
    create_table :bets do |t|
      t.bigint :game_id
      t.bigint :player_id
      t.string :uid
      t.string :state
      t.timestamps
    end
  end
end
