# frozen_string_literal: true

class CreateSlots < ActiveRecord::Migration[6.0]
  def change
    create_table :slots do |t|
      t.string :name, index: [:game_id]
      t.string :uid, index: [:game_id]
      t.integer :game_id
      t.timestamps
    end
  end
end
