# frozen_string_literal: true

guard :rspec, cmd: 'bundle exec rspec', all_after_pass: true, all_on_start: true do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^comparators/(.+)\.rb$}) { |m| "spec/comparators/#{m[1]}_spec.rb" }
  watch(%r{^combinations/(.+)\.rb$}) { |m| "spec/combinations/#{m[1]}_spec.rb" }
  watch(%r{^engine/(.+)\.rb$}) { |m| "spec/engine/#{m[1]}_spec.rb" }
  watch(%r{^extensions/(.+)\.rb$}) { |m| "spec/extensions/#{m[1]}_spec.rb" }
  watch(%r{^app/(.+)\.rb$}) { |m| "spec/lib/#{m[1]}_spec.rb" }
  watch(%r{^app/(.+)/(.+)\.rb$}) { |m| "spec/#{m[1]}/#{m[2]}_spec.rb" }
  watch('spec/spec_helper.rb') { 'spec' }
end

guard 'annotate', :factories => false, :show_indexes => false do
  watch( 'db/schema.rb' )
  watch( 'app/models/**/*.rb' )
end

guard :rubocop, cli: ['-a'] do
  watch(%r{.+\.rb$})
  watch(%r{(?:.+/)?\.rubocop(?:_todo)?\.yml$}) { |m| File.dirname(m[0]) }
end
