# frozen_string_literal: true

class LineSlotWithWildChanceTable
  attr_reader :reels, :total_slots_qty, :data, :all_slots
  def initialize(reels, all_slots)
    @reels = reels
    @all_slots = all_slots
    @data = {}
    @total_slots_qty = reels.map(&:slot_ids).map(&:count).map(&:to_f)
  end

  def chance(slot, reel_position)
    data[slot.uid] ||= []
    data[slot.uid][reel_position] ||= slot_with_wilds_qty(slot, reel_position) / total_slots_qAnemty[reel_position]
  end

  def print
    all_slots.each do |slot|
      reels.each_with_index do |_reel, index|
        chance(slot, index)
      end
    end
    pp 'SlotWithWildChanceTable results'
    pp data
  end

  private

  def slot_with_wilds_qty(slot, reel_position)
    fitting_slots = [slot]
    fitting_slots += slot.wilds
    fs_ids = fitting_slots.pluck(:id).uniq.compact

    reels[reel_position].slot_ids.count { |s_id| fs_ids.include?(s_id) }
  end
end
