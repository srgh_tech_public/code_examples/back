# frozen_string_literal: true

class BuildRtpService
  attr_reader :game_rtp, :slots, :reels, :game, :wilds, :scatters, :line_slots
  def initialize(game_rtp)
    @game_rtp = game_rtp
    @game = game_rtp.game
    @slots = game_rtp.game.slots.includes(:wilds)
    @reels = @game_rtp.reels.to_a
    @scatters = game.scatters
    @wilds = game.wilds
    @line_slots = slots - scatters - wilds
    @slot_chances_table = []
  end

  def call
    reels.each { |reel| reel.update(slot_ids: initial_reel_fill(reel.position)) }
    line_statistics
  end

  def line_statistics
    puts '######### Slot chance per reel including wilds #########'

    chance_multiplier_line_table = ChanceMultiplierLineTable.new(reels, slots)
    chance_multiplier_line_table.print
  end

  private

  def initial_reel_fill(reel_position)
    slots.reject { |slot| slot.denied_on_reel?(reel_position) }
         .pluck(:id).shuffle
  end
end
