# frozen_string_literal: true

class ChanceMultiplierLineTable
  attr_reader :combination_chance_table, :reels, :data, :all_slots
  def initialize(reels, all_slots)
    @reels = reels
    @combination_chance_table = CombinationChanceTable.new(reels, all_slots)
    @all_slots = all_slots
    @data = {}
  end

  def chance(slot, position = nil)
    return data.dig(slot.uid, position) if data[slot.uid] && position
    return data.dig(slot.uid) if data[slot.uid]

    data[slot.uid] = []
    max_reel_index = reels.count - 1
    0.upto(max_reel_index) do |index|
      data[slot.uid] << slot.line_multipliers.dig(index).to_f * combination_chance_table.chance(slot, index)
    end
    return data.dig(slot.uid, position) if position

    data.dig(slot.uid)
  end

  def print
    combination_chance_table.print
    pp 'ChanceMultiplierLineTable results'
    all_slots.each do |slot|
      chance(slot)
    end
    pp data
  end
end
