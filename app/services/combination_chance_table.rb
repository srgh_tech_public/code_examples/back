# frozen_string_literal: true

class CombinationChanceTable
  attr_reader :slot_chance_table, :reels, :data, :all_slots
  def initialize(reels, all_slots)
    @reels = reels
    @slot_chance_table = LineSlotWithWildChanceTable.new(reels, all_slots)
    @all_slots = all_slots
    @data = {}
  end

  def chance(slot, length)
    return data.dig(slot.uid, length) if data[slot.uid]

    data[slot.uid] = []
    chance = 1
    max_reel_index = reels.count - 1
    0.upto(max_reel_index) do |index|
      chance *= slot_chance_table.chance(slot, index)
      if index == max_reel_index
        data[slot.uid][index] = chance
      else
        data[slot.uid][index] = chance - chance * slot_chance_table.chance(slot, index + 1)
      end
    end
  end

  def print
    slot_chance_table.print
    pp 'CombinationChanceTable results'
    all_slots.each do |slot|
      chance(slot, 1)
    end
    pp data
  end
end
