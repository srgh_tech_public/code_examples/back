# frozen_string_literal: true

# == Schema Information
#
# Table name: reels
#
#  id          :bigint           not null, primary key
#  game_rtp_id :bigint
#  slot_ids    :bigint           default([]), not null, is an Array
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Reel < ApplicationRecord
  belongs_to :game_rtp, optional: false
  has_many :reel_slots, dependent: :destroy
  has_many :positioned_slots, through: :reel_slots, class_name: 'Slot'
  has_one :game, through: :game_rtp
  validate :slot_should_belong_to_game
  validate :slot_allowed_to_be_on_reel

  def position
    return @position if @position

    @position = game_rtp.reel_ids.index(id) + 1
  end

  private

  def ordered_slots
    Slot.where(id: slot_ids.uniq.sort)
        .includes(:game)
        .order(id: :asc)
  end

  # TODO: checks should be performed here
  def slot_allowed_to_be_on_reel
    return unless Slot.where(id: slot_ids)
                      .map { |slot| slot.denied_on_reel?(position) }
                      .any?

    errors.add(:slot_ids, "Slot can't be located on this reel")
  end

  def slot_should_belong_to_game
    return if ordered_slots.map { |slot| slot.game == game }.all?

    errors.add(:slot_ids, 'Slot should belong to the same game as reel')
  end
end
