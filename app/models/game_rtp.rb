# frozen_string_literal: true

# == Schema Information
#
# Table name: game_rtps
#
#  id         :bigint           not null, primary key
#  game_id    :integer
#  active     :boolean          default(FALSE)
#  rtp        :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  details    :jsonb
#

class GameRtp < ApplicationRecord
  validates :rtp, presence: true,
                  numericality: {
                    greater_than: 50,
                    less_than: 110,
                    only_integer: false
                  }
  has_many :reels, -> { order(id: :asc) }, dependent: :destroy
  belongs_to :game, optional: false
  after_create :generate_empty_reels
  delegate :reels_qty, :rows_qty, to: :game
  delegate :slots, to: :game, prefix: true

  private

  def generate_empty_reels
    reels_qty.times { reels.create }
  end
end
