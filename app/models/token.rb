# frozen_string_literal: true

# == Schema Information
#
# Table name: tokens
#
#  id          :bigint           not null, primary key
#  player_id   :bigint
#  platform_id :bigint
#  value       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Token < ApplicationRecord
  validates :value, presence: true, uniqueness: true
  belongs_to :platform, optional: false
  belongs_to :player, optional: false
  has_many :actions, dependent: :destroy
end
