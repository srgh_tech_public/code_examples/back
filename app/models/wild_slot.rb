# frozen_string_literal: true

# == Schema Information
#
# Table name: wild_slots
#
#  wild_id        :bigint
#  simple_slot_id :integer
#

class WildSlot < ApplicationRecord
  belongs_to :simple_slot, class_name: 'Slot'
  belongs_to :wild, class_name: 'Slot'
end
