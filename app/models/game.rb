# frozen_string_literal: true

# == Schema Information
#
# Table name: games
#
#  id         :bigint           not null, primary key
#  name       :string
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Game < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :uid, presence: true, uniqueness: true
  has_many :slots, dependent: :destroy
  has_many :game_rtps
  has_many :reels, through: :game_rtps
  has_many :scatters, -> { scatters }, class_name: 'Slot'
  has_many :wilds, -> { wilds }, class_name: 'Slot'

  def reels_qty
    5
  end

  def rows_qty
    3
  end
end
