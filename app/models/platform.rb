# frozen_string_literal: true

# == Schema Information
#
# Table name: platforms
#
#  id         :bigint           not null, primary key
#  uid        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Platform < ApplicationRecord
  validates :uid, presence: true, uniqueness: true
  has_many :players, dependent: :destroy
  has_many :bets, through: :players
  has_many :tokens, through: :players
end
