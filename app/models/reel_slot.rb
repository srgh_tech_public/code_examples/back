# frozen_string_literal: true

# == Schema Information
#
# Table name: reel_slots
#
#  id                 :bigint
#  reel_id            :bigint
#  positioned_slot_id :bigint
#

class ReelSlot < ApplicationRecord
  belongs_to :reel
  belongs_to :positioned_slot, class_name: 'Slot'
end
