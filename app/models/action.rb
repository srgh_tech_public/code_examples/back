# frozen_string_literal: true

# == Schema Information
#
# Table name: actions
#
#  id         :bigint           not null, primary key
#  bet_id     :bigint
#  token_id   :bigint
#  data       :json
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Action < ApplicationRecord
  belongs_to :bet, optional: false
  belongs_to :token, optional: false
  has_one :action_result, dependent: :destroy
end
