# frozen_string_literal: true

# == Schema Information
#
# Table name: slots
#
#  id                    :bigint           not null, primary key
#  name                  :string
#  uid                   :string
#  game_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  slot_ids              :integer          default([]), not null, is an Array
#  line_multipliers      :integer          default([]), not null, is an Array
#  scatter_multipliers   :integer          default([]), not null, is an Array
#  denied_reel_positions :integer          default([]), not null, is an Array
#

class Slot < ApplicationRecord
  validates :name, presence: true, uniqueness: { scope: :game_id }
  validates :uid, presence: true, uniqueness: { scope: :game_id }
  validate :max_line_multipliers
  validate :max_scatter_multipliers

  belongs_to :game, optional: false
  has_one :line_combination
  has_many :reel_slots, foreign_key: :positioned_slot_id
  has_many :reels, through: :reel_slots
  has_many :wild_slots, class_name: 'WildSlot', foreign_key: 'wild_id'
  has_many :w_slots, class_name: 'WildSlot', foreign_key: 'simple_slot_id'
  has_many :wilds, through: :w_slots, class_name: 'Slot'
  has_many :simple_slots, through: :wild_slots, class_name: 'Slot'
  delegate :reels_qty, :rows_qty, to: :game

  scope :wilds, lambda {
    where(
      Arel::Nodes::NamedFunction
        .new('array_length', [arel_table[:slot_ids], 1])
        .gt(0)
    )
  }
  scope :scatters, lambda {
    where(
      Arel::Nodes::NamedFunction
        .new('array_length', [arel_table[:scatter_multipliers], 1])
        .gt(0)
    )
  }

  def denied_on_reel?(position)
    denied_reel_positions.include?(position)
  end

  def substitutes?(slot)
    simple_slots.include?(slot)
  end

  private

  def max_line_multipliers
    return if line_multipliers.empty?
    return if line_multipliers.size == reels_qty

    errors.add(:line_multipliers, 'invalid qty')
  end

  # TODO: We need a mechanism to check in a better way
  def max_scatter_multipliers
    return if scatter_multipliers.empty?
    return if scatter_multipliers.size <= reels_qty

    errors.add(:scatter_multipliers, 'invalid qty')
  end
end
