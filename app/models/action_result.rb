# frozen_string_literal: true

# == Schema Information
#
# Table name: action_results
#
#  id         :bigint           not null, primary key
#  action_id  :bigint
#  data       :json
#  status     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ActionResult < ApplicationRecord
  belongs_to :action
  has_one :bet, through: :action
  has_one :player, through: :bet
end
