# frozen_string_literal: true

# == Schema Information
#
# Table name: players
#
#  id          :bigint           not null, primary key
#  username    :string
#  platform_id :bigint
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Player < ApplicationRecord
  validates :username, presence: true, uniqueness: { scope: :platform_id }
  belongs_to :platform, optional: false
  has_many :bets, dependent: :destroy
  has_many :tokens, dependent: :destroy
  has_many :actions, through: :bets
  has_many :action_results, through: :actions
end
